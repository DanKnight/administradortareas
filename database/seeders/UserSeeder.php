<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datos = [
            [
                'nombreUser' => 'juan',
                'email' => 'administrador@administrador.com',
                'password' => Hash::make('123123'),
                'id_grupo' => 1,
                'id_tipoUsuario' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],[
                'nombreUser' => 'jose',
                'email' => 'tecnico@tecnico.com',
                'password' => Hash::make('123123'),
                'id_grupo' => 1,
                'id_tipoUsuario' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],[
                'nombreUser' => 'alberto',
                'email' => 'alberto@administrador.com',
                'password' => Hash::make('123123'),
                'id_grupo' => 2,
                'id_tipoUsuario' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],[
                'nombreUser' => 'jorge',
                'email' => 'jorge@tecnico.com',
                'password' => Hash::make('123123'),
                'id_grupo' => 1,
                'id_tipoUsuario' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],[
                'nombreUser' => 'pedro',
                'email' => 'pedro@tecnico.com',
                'password' => Hash::make('123123'),
                'id_grupo' => 1,
                'id_tipoUsuario' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],[
                'nombreUser' => 'javier',
                'email' => 'javier@tecnico.com',
                'password' => Hash::make('123123'),
                'id_grupo' => 2,
                'id_tipoUsuario' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],[
                'nombreUser' => 'pablo',
                'email' => 'pablo@tecnico.com',
                'password' => Hash::make('123123'),
                'id_grupo' => 2,
                'id_tipoUsuario' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],[
                'nombreUser' => 'francisco',
                'email' => 'francisco@tecnico.com',
                'password' => Hash::make('123123'),
                'id_grupo' => 2,
                'id_tipoUsuario' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ];

        DB::table('users')->insert($datos);
    }
}
