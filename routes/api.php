<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\GrupoController;
use App\Http\Controllers\TipoUsuarioController;
use App\Http\Controllers\TareaStatusController;
use App\Http\Controllers\TareaController;
use App\Http\Controllers\UsuarioTareaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', [UserController::class, 'login']);
Route::post('/registro', [UserController::class, 'registro']);

Route::post('/grupo', [GrupoController::class, 'registrar']);
Route::post('/tipousuario', [TipoUsuarioController::class, 'registrar']);
Route::post('/status', [TareaStatusController::class, 'registrar']);
Route::post('/tarea', [TareaController::class, 'registrar']);

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('/usuarios', [UserController::class, 'listarUsuarios']);
    Route::get('/listargrupo', [GrupoController::class, 'listarGrupoUsuario']);
    Route::get('/grupos', [GrupoController::class, 'listarGrupos']);
    Route::get('/todosgrupos', [GrupoController::class, 'listarTodosGrupos']);
    Route::post('/asignar', [UsuarioTareaController::class, 'asignarTarea']);
    Route::get('/listartareas', [TareaController::class, 'listarTareas']);
    Route::post('/completa', [TareaController::class, 'tareaCompleta']);
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


