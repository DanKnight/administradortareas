<?php

namespace App\Http\Controllers;

use App\Models\TipoUsuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TipoUsuarioController extends Controller
{
    // Declaracion de status para las peticiones
    private $success = 200;
    private $error = 404;

    // Registrar un tipo de usuario
    public function registrar(Request $request) {
        // Validar los campos del request
        $validacion = Validator::make($request->all(), [
            'nombre' => 'required|string',
        ]);

        if ($validacion->fails()) {
            return response($validacion->errors()->toJson(), $this->error);
        }

        // Crear tipo de usuario
        $tipoUsuario = TipoUsuario::create([
            'nombreTipo' => $request->get('nombre'),
        ]);

        // Retornar respuesta json
        $mensaje = 'Registro agregado correctamente';

        return response()->json(['data' => $tipoUsuario, 'mensaje' => $mensaje, 'status' => $this->success]);
    }
}
