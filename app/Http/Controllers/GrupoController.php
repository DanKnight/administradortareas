<?php

namespace App\Http\Controllers;

use App\Models\Grupo;
use App\Models\StatusTarea;
use App\Models\Tareas;
use App\Models\User;
use App\Models\UsuarioTarea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class GrupoController extends Controller
{
    // Declaracion de status para las peticiones
    private $success = 200;
    private $error = 404;

    // Registrar grupo
    public function registrar(Request $request) {
        // Validar los campos del request
        $validacion = Validator::make($request->all(), [
            'nombre' => 'required|string',
        ]);

        if ($validacion->fails()) {
            return response($validacion->errors()->toJson(), $this->error);
        }

        // Crear grupo
        $grupo = Grupo::create([
            'nombreGrupo' => $request->get('nombre'),
        ]);

        // Retornar respuesta json
        $mensaje = 'Registro agregado correctamente';

        return response()->json(['data' => $grupo, 'mensaje' => $mensaje, 'status' => $this->success]);
    }

    // Obtener los usuarios que pertenecen a un grupo en especficico
    public function listarGrupoUsuario() {
        // Decodificacion del token para verificar tipo de usuario
        $datos = JWTAuth::parseToken()->authenticate();
        $tipoUsuario = User::find($datos->id)->tipo;

        if ($tipoUsuario->nombreTipo == "Administrador"){
            // Obtener usuarios que pertenecen a determinado grupo por medio de una relacion
            $grupo = Grupo::find($datos->id_grupo)->usuarios;

            $datosUsuario = array();

            // Recorrer usuarios obtenidos para guardar los datos en el array
            foreach ($grupo as $usuario){
                array_push($datosUsuario, [
                    'id' => $usuario->id,
                    'nombre' => $usuario->nombreUser,
                    'email' => $usuario->email,
                ]);
            }

            // Retornar respuesta json
            $mensaje = 'Informacion Encontrada';

            return response()->json(['data' => $datosUsuario, 'mensaje' => $mensaje, 'status' => $this->success]);
        }

        // Retornar respuesta json
        $mensaje = 'Informacion incorrecta';

        return response()->json(['data' => null, 'mensaje' => $mensaje, 'status' => $this->error]);
    }

    // Obtener los grupos
    public function listarGrupos() {
        // Consulta para obtener todos los grupos existentes en la tabla que no hayan sido eliminados
        $grupos = Grupo::all()->toArray();
        if ($grupos) {
            // Retornar respuesta json
            $mensaje = 'Informacion Encontrada';

            return response()->json(['data' => $grupos, 'mensaje' => $mensaje, 'status' => $this->success]);
        }
        // Retornar respuesta json
        $mensaje = 'Informacion no encontrada';

        return response()->json(['data' => null, 'mensaje' => $mensaje, 'status' => $this->error]);
    }

    // listar los grupos con sus respectivas tareas y usuarios
    public function listarTodosGrupos() {
        // Decodificacion del token para verificar tipo de usuario
        $datos = JWTAuth::parseToken()->authenticate();
        $tipoUsuario = User::find($datos->id)->tipo;

        if ($tipoUsuario->nombreTipo == "Administrador"){
            // Obtener todos los grupos de la tabla
            $grupos = Grupo::all();

            // Obtener todas las tareas de la tabla
            $arrayTareas = Tareas::all();

            // Obtener todos los registros de las tablas y convertirlos en array para optimizar la busqueda de informacion
            $arrayStatus = StatusTarea::all()->toArray();

            $usuariosTareas = UsuarioTarea::all()->toArray();

            $arrayUsuarios = User::all()->toArray();

            /*
            * Funciones buscar informacion en los array obtenidos en lugar de hacer 
            * busqueda en directamente en la base de datos mediante eloquent
            */
            function buscarStatus($codigo, $array) {
                foreach ($array as $key => $val) {
                    if ($val['id'] === $codigo) {
                        return $val['statusTarea'];
                    }
                }
            }

            function buscarUsuario($codigo, $array1, $array2) {
                $usuarioAsignados = array();
                foreach ($array1 as $key => $val) {
                    if ($val['id_tarea'] === $codigo) {
                        foreach ($array2 as $key1 => $val1) {
                            if ($val1['id'] === $val['id_user']) {
                                array_push($usuarioAsignados, [
                                    'nombre' => $val1['nombreUser'],
                                ]);
                            }
                        }
                    }
                }
                return $usuarioAsignados;
            }

            function buscarTarea($codigo, $array) {
                $tareasAsignados = array();

                foreach ($array as $key => $val) {
                    if ($val['grupo'] === $codigo) {
                        array_push($tareasAsignados, $val);
                    }
                }
                return $tareasAsignados;
            }

            function buscarUsuarioGrupo($id){
                $grupo = Grupo::find($id)->usuarios;

                $datosUsuario = array();

                if ($grupo){
                    foreach ($grupo as $usuario){
                        array_push($datosUsuario, [
                            'id' => $usuario->id,
                            'nombre' => $usuario->nombreUser,
                            'email' => $usuario->email,
                        ]);
                    }
                }

                return $datosUsuario;
            }

            $datosTareas = array();

            $datosGrupos = array();

            // Recorrer tareas para asignar usuarios correspondientes
            foreach ($arrayTareas as $tarea){
                $usuarioAsignados = buscarUsuario($tarea->id, $usuariosTareas, $arrayUsuarios);
                $status = buscarStatus($tarea->id_status, $arrayStatus);

                array_push($datosTareas, [
                    'id' => $tarea->id,
                    'nombre' => $tarea->nombreTarea,
                    'fecha' => $tarea->fechaTarea,
                    'grupo' => $tarea->id_grupo,
                    'status' => $status,
                    'usuariosAsignados' => $usuarioAsignados
                ]);
            }

            // Recorrer grupos para asignar taeas y usuarios correspondientes
            foreach ($grupos as $grupo){
                $grupoAsignado = buscarTarea($grupo->id, $datosTareas);
                $usuariosGrupo = buscarUsuarioGrupo($grupo->id);

                array_push($datosGrupos, [
                    'id' => $grupo->id,
                    'nombre' => $grupo->nombreGrupo,
                    'tareasAsignadas' => $grupoAsignado,
                    'usuariosGrupo' => $usuariosGrupo,
                ]);
            }

            // Retornar respuesta json
            $mensaje = 'Informacion Encontrada';

            return response()->json(['data' => $datosGrupos, 'mensaje' => $mensaje, 'status' => $this->success]);
        }

        // Retornar respuesta json
        $mensaje = 'Informacion incorrecta';

        return response()->json(['data' => null, 'mensaje' => $mensaje, 'status' => $this->error]);
    }
}
