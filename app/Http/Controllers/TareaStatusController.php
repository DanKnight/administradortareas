<?php

namespace App\Http\Controllers;


use App\Models\StatusTarea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TareaStatusController extends Controller
{
    // Declaracion de status para las peticiones
    private $success = 200;
    private $error = 404;

    // Registrar un status
    public function registrar(Request $request) {
        // Validar los campos del request
        $validacion = Validator::make($request->all(), [
            'nombre' => 'required|string',
        ]);

        if ($validacion->fails()) {
            return response($validacion->errors()->toJson(), $this->error);
        }

        // Crear status
        $status = StatusTarea::create([
            'statusTarea' => $request->get('nombre'),
        ]);

        // Retornar respuesta json
        $mensaje = 'Registro agregado correctamente';

        return response()->json(['data' => $status, 'mensaje' => $mensaje, 'status' => $this->success]);
    }
}
