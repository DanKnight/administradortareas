<?php

namespace App\Http\Controllers;


use App\Models\Grupo;
use App\Models\StatusTarea;
use App\Models\Tareas;
use App\Models\User;
use App\Models\UsuarioTarea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class TareaController extends Controller
{
    // Declaracion de status para las peticiones
    private $success = 200;
    private $error = 404;

    // Registrar nueva tarea
    public function registrar(Request $request) {
        // Validar los campos del request
        $validacion = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'fecha' => 'required',
            'grupo' => 'required',
            'status' => 'required',
        ]);

        if ($validacion->fails()) {
            return response($validacion->errors()->toJson(), $this->error);
        }

        // Crear tarea
        $tarea = Tareas::create([
            'nombreTarea' => $request->get('nombre'),
            'fechaTarea' => $request->get('fecha'),
            'id_grupo' => $request->get('grupo'),
            'id_status' => $request->get('status'),
        ]);

        // Retornar respuesta json
        $mensaje = 'Registro agregado correctamente';

        return response()->json(['data' => $tarea, 'mensaje' => $mensaje, 'status' => $this->success]);
    }

    // Obtener todas las tareas asignadas a un grupo
    public function listarTareas() {
        // Decodificacion del token para verificar tipo de usuario
        $datos = JWTAuth::parseToken()->authenticate();
        $tipoUsuario = User::find($datos->id)->tipo;

        if ($tipoUsuario->nombreTipo != "Administrador"){
            $tareas = Grupo::find($datos->id_grupo)->tareas;

            if (count($tareas) != 0){
                // Obtener todos los registros de las tablas y convertirlos en array para optimizar la busqueda de informacion
                $arrayStatus = StatusTarea::all()->toArray();

                $usuariosTareas = UsuarioTarea::all()->toArray();

                $arrayUsuarios = User::all()->toArray();

                /*
                * Funciones buscar informacion en los array obtenidos en lugar de hacer 
                * busqueda en directamente en la base de datos mediante eloquent
                */
                function buscarStatus($codigo, $array) {
                    foreach ($array as $key => $val) {
                        if ($val['id'] === $codigo) {
                            return $val['statusTarea'];
                        }
                    }
                }

                function buscarUsuario($codigo, $array1, $array2) {
                    $usuarioAsignados = array();
                    foreach ($array1 as $key => $val) {
                        if ($val['id_tarea'] === $codigo) {
                            foreach ($array2 as $key1 => $val1) {
                                if ($val1['id'] === $val['id_user']) {
                                    array_push($usuarioAsignados, [
                                        'nombre' => $val1['nombreUser'],
                                    ]);
                                }
                            }
                        }
                    }
                    return $usuarioAsignados;
                }

                $datosTareas = array();

                // Recorrer tareas para asignar usuarios correspondientes
                foreach ($tareas as $tarea){
                    $usuarioAsignados = buscarUsuario($tarea->id, $usuariosTareas, $arrayUsuarios);
                    $status = buscarStatus($tarea->id_status, $arrayStatus);

                    array_push($datosTareas, [
                        'id' => $tarea->id,
                        'nombre' => $tarea->nombreTarea,
                        'fecha' => $tarea->fechaTarea,
                        'status' => $status,
                        'usuariosAsignados' => $usuarioAsignados
                    ]);
                }

                // Retornar respuesta json
                $mensaje = 'Informacion Encontrada';

                return response()->json(['data' => $datosTareas, 'mensaje' => $mensaje, 'status' => $this->success]);
            }

            // Retornar respuesta json
            $mensaje = 'No se encontraron tareas disponibles';

            return response()->json(['data' => null, 'mensaje' => $mensaje, 'status' => $this->error]);

        }

        // Retornar respuesta json
        $mensaje = 'Informacion incorrecta';

        return response()->json(['data' => $datos, 'mensaje' => $mensaje, 'status' => $this->error]);
    }

    // Marcar una tarea com completada
    public function tareaCompleta(Request $request) {
        // Obtener tarea por medio del id
        $tarea = Tareas::where('id', $request->get('id'))->first();

        if ($tarea){
            // Cambiar status y eliminar tarea
            $tarea->id_status = 3;
            $tarea->save();
            $tarea->delete();
            // Buscar relacion de usuario y tarea
            $tareasUsuario = UsuarioTarea::where('id_tarea', $request->get('id'))->get();

            if (count($tareasUsuario) != 0){
                // Recorrer informacion obtenida en caso de que existan varias relacion sobre una tarea
                foreach ($tareasUsuario as $tareasU){
                    $tareasU->delete();
                }
            }

            // Retornar respuesta json
            $mensaje = 'Registro eliminado correctamente';

            return response()->json(['data' => $tarea, 'mensaje' => $mensaje, 'status' => $this->success]);
        }
        // Retornar respuesta json
        $mensaje = 'Informacion incorrecta';

        return response()->json(['data' => $tarea, 'mensaje' => $mensaje, 'status' => $this->error]);
    }
}
