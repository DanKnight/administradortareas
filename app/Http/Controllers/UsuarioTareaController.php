<?php

namespace App\Http\Controllers;


use App\Models\Tareas;
use App\Models\UsuarioTarea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsuarioTareaController extends Controller
{
    // Declaracion de status para las peticiones
    private $success = 200;
    private $error = 404;

    // Funcion para asignar tarea
    public function asignarTarea(Request $request) {
        // Validar los campos del request
        $validacion = Validator::make($request->all(), [
            'tarea' => 'required',
            'usuario' => 'required',
        ]);

        if ($validacion->fails()) {
            return response($validacion->errors()->toJson(), $this->error);
        }

        // Verificar que si existe una relacion entre el usuario y la tarea
        $existe = UsuarioTarea::where('id_tarea', $request->get('tarea'))->where('id_user', $request->get('usuario'))->first();

        if ($existe) {
            // Retornar respuesta json
            $mensaje = 'El usuario ya ha sido asignado a esta tarea';
            
            return response()->json(['data' => $existe, 'mensaje' => $mensaje, 'status' => $this->error]);
        }

        // Actualizar status a de la tarea
        $tarea = Tareas::where('id', $request->get('tarea'))->first();
        $tarea->id_status = 2;
        $tarea->save();

        // Crear relacion entre usuario y tarea
        $asignar = UsuarioTarea::create([
            'id_tarea' => $request->get('tarea'),
            'id_user' => $request->get('usuario'),
        ]);

        // Retornar respuesta json
        $mensaje = 'Registro agregado correctamente';

        return response()->json(['data' => $asignar, 'mensaje' => $mensaje, 'status' => $this->success]);
    }

}
