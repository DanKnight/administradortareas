<?php

namespace App\Http\Controllers;

use App\Models\Grupo;
use App\Models\StatusTarea;
use App\Models\Tareas;
use App\Models\TipoUsuario;
use App\Models\User;
use App\Models\UsuarioTarea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    // Declaracion de status para las peticiones
    private $success = 200;
    private $error = 404;

    // Realizar el login del usuario
    public function login(Request $request){
        // Obtener los datos del request
        $user = $request->only('email', 'password');

        try {
            // verificar si los datos ingresados existen para generar un token
            if (!$token = JWTAuth::attempt($user)) {
                return response()->json(['error' => 'Credenciales invalidas'], $this->error);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'No se genero token'], $this->error);
        }

        // Retornar respuesta json
        return response()->json(['token' => $token]);
    }

    // Realizar registro de usuarios
    public function registro(Request $request)
    {
        // Validar los campos del request
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'grupo' => 'required',
            'tipo' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), $this->error);
        }

        // Crear usuario
        $user = User::create([
            'nombreUser' => $request->get('nombre'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'id_grupo' => $request->get('grupo'),
            'id_tipoUsuario' => $request->get('tipo'),
        ]);

        // Retornar respuesta json
        $token = JWTAuth::fromUser($user);

        return response()->json(['data' => $user,'token' => $token, 'status' => $this->success]);
    }

    // Obtener todos los usuarios
    public function listarUsuarios() {
        // Obtener todos los usuarios existentes en la tabla
        $usuarios = User::all()->toArray();

        if ($usuarios) {
            // Retornar respuesta json
            $mensaje = 'Informacion Encontrada';

            return response()->json(['data' => $usuarios, 'mensaje' => $mensaje, 'status' => $this->success]);
        }
        // Retornar respuesta json
        $mensaje = 'Informacion no encontrada';

        return response()->json(['data' => null, 'mensaje' => $mensaje, 'status' => $this->error]);
    }
}
