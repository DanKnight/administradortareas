<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoUsuario extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'nombreTipo',
    ];

    public function usuarios() {
        return $this->hasMany(User::class, 'id_tipoUsuario', 'id');
    }
}
