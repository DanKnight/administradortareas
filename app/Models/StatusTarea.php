<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatusTarea extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'statusTarea',
    ];

    public function tareas() {
        return $this->hasMany(Tareas::class, 'id_status', 'id');
    }
}
