<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsuarioTarea extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'id_tarea',
        'id_user'
    ];

    public function tarea() {
        return $this->belongsTo(Tareas::class, 'id', 'id_tarea');
    }

    public function usuario() {
        return $this->hasMany(User::class, 'id', 'id_user');
    }
}
