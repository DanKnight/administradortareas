<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Grupo extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'id';

    protected $fillable = [
        'nombreGrupo',
    ];

    public function usuarios() {
        return $this->hasMany(User::class, 'id_grupo', 'id');
    }

    public function tareas() {
        return $this->hasMany(Tareas::class, 'id_grupo', 'id');
    }
}
