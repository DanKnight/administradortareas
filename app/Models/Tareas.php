<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tareas extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'nombreTarea',
        'fechaTarea',
        'id_grupo',
        'id_status',
    ];

    public function grupo() {
        return $this->hasOne(Grupo::class, 'id', 'id_grupo');
    }

    public function status() {
        return $this->hasOne(StatusTarea::class, 'id', 'id_status');
    }

    public function tareaUsuario() {
        return $this->hasOne(UsuarioTarea::class, 'id_tarea', 'id');
    }
}
