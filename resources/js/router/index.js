import React from 'react';
import {Switch, BrowserRouter, Route} from "react-router-dom";

import Home from '../components/Home';
import PublicRoute from "./publicRoute";
import PrivateRoute from "./privateRoute";
import Dashboard from "../components/Dashboard";
import RegistrarTarea from "../components/RegistrarTarea";

function Router(props) {
    return (
        <BrowserRouter>
            <Switch>
                <PublicRoute restricted={true} component={Home} path="/" exact/>
                <PrivateRoute component={Dashboard} path="/dashboard" exact/>
                <PrivateRoute component={RegistrarTarea} path="/registrar" exact/>
            </Switch>
        </BrowserRouter>
    );
}

export default Router;
