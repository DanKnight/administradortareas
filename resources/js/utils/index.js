// Constante para realizar la redirecciona de las paginas
const web = 'http://localhost:8000/';
//const web = 'https://admintareas-er.000webhostapp.com/';

// Constantes para realizar peticiones a la pi
export const Api = {
    url: 'http://localhost:8000/api/'
    //url: 'https://admintareas-er.000webhostapp.com/api/'
}

// Funcion para crear el header de las peticiones que requiren token
export function autenticacionHeader() {
    // token del usuario conectado
    let token = localStorage.getItem('token');
    // verifica que el token exista con su token
    if (token) {
        // regresa la cabecera con su token de autorización
        return { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token };
    } else {
        // regresa un objeto vacío en caso de que el token no exista
        return {};
    }
}

// Funcion para guardar token en localstorage al iniciar sesion
export const login = (token) => {
    localStorage.setItem('token', token);
}

// Funcion para eliminar token al cerrar sesion
export const logout = () => {
    localStorage.removeItem('token');
    location.href = `${web}`;
}

// Funcion para verificar que exista un token y poder realizar peticiones a la api
export const isLogin = () => {
    if (localStorage.getItem('token')) {
        return true;
    }
    return false;
}

// Funcion para formar la url a la que sera redireccionada la pagina
export const redireccionWeb = (url) => {
    return `${web}${url}`;
}
