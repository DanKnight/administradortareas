import React, {Component} from 'react';
import {Api, autenticacionHeader} from '../utils';
import {Badge, Button, Card, CardColumns, ListGroup, ListGroupItem, Navbar} from "react-bootstrap";
import axios from "axios";

export default class Grupo extends Component {
    constructor(props) {
        super(props);

        // Obtencion del array de tareas por medio del props
        const grupoArray = props.grupoArray;

        this.state = {
            grupo: grupoArray
        };
    }

    render() {
        const grupo = this.state.grupo;

        // Generacion de componente de manera dinamica por medio de la funcion map
        return (
            <div className="container">
                {
                    grupo.length != 0 ?
                        <CardColumns>
                            {
                                grupo.map((usuario, key) =>
                                    <Card style={{ width: '18rem' }} key={key}>
                                        <Card.Body>
                                            <Card.Title>Nombre: {usuario.nombre}</Card.Title>
                                            <Card.Subtitle className="mb-2 text-muted">Email: {usuario.email}</Card.Subtitle>
                                        </Card.Body>
                                    </Card>
                                )
                            }
                        </CardColumns>
                        :
                        <h2 className="text-center">No hay usuarios</h2>
                }

            </div>
        );
    }
}
