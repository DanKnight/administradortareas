import React, {Component} from 'react';
import {Api, autenticacionHeader, redireccionWeb} from '../utils';
import {Alert, Button, Form, FormGroup, Navbar} from "react-bootstrap";
import axios from "axios";
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

export default class RegistrarTarea extends Component {
    constructor(props) {
        super(props);

        // Declaracion de funcion
        this.obtenerFecha = this.obtenerFecha.bind(this);

        this.state = {
            grupos: null,
            fecha: null,
            grupo: 1,
            mensaje: ""
        }

        // Ejecucion de funciones al cargar la pagina
        this.listarGrupos();
    }

    // Peticion para obtener la lista de grupos
    listarGrupos = () => {
        // Obtencion de autorizacion para peticiones
        let auth = autenticacionHeader();

        axios.get(`${Api.url}grupos`, {headers: auth}).then((response) => {
            if (response.data.status == 200){
                // Actualizar el state con las tareas que no han sido completadas
                this.setState({
                    grupos: response.data.data,
                });
            }
        }).catch((error) => {
            console.log(error);
        })
    }

    // Funcion para agregar los datos de formulario al state
    cargardatos = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        let data = {};
        data[name] = value;
        this.setState(data);
    };

    // Funcion para obtener la fecha seleccionada
    obtenerFecha(dia) {
        // Mandar fecha al state y darle formato (dd/mm/aaaa)
        this.setState({fecha: dia.toLocaleDateString()});
    }

    // Funcion para redireccionar pagina al cancelar
    cancelar = () => {
        location.href = redireccionWeb('dashboard');
    }

    // Funcion para agregar nueva tarea
    registrar = () => {
        // Obtencion de autorizacion para peticiones
        let auth = autenticacionHeader();

        // Separacion de fecha para darle el formato del tipo de dato date de mysql (aaaa/mm/dd)
        var anio = this.state.fecha.slice(-4);
        var mes = this.state.fecha.slice(-7,-5);
        var dia = this.state.fecha.slice(0,2);

        var fecha = anio+'-'+mes+'-'+dia;

        // Generacion de parametros para la peticion
        let parametros = {nombre: this.state.nombre, fecha: fecha, grupo: this.state.grupo, status: 1};

        axios.post(`${Api.url}tarea`, parametros, {headers: auth}).then((response) => {
            if (response.data.status == 200){
                location.href = redireccionWeb('dashboard');
            }
        }).catch(error => {
            // Mensaje enviado al state para generar alerta
            this.setState({
                mensaje: "Datos Incorrectos"
            });
        });
    };

    render() {
        let grupos = this.state.grupos;
        const mensaje = this.state.mensaje;

        // Generacion de componente de manera dinamica por medio de la funcion map
        return (
            <div>
                <div>
                    <Navbar bg="dark" variant="dark">
                        <Navbar.Brand>Administrador de Tareas</Navbar.Brand>
                        <Navbar.Toggle/>
                        <Navbar.Collapse className="justify-content-end">
                            <Button onClick={this.logout} variant="secondary">Cerrar Sesión</Button>
                        </Navbar.Collapse>
                    </Navbar>
                </div>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-header text-center">Registrar Tarea</div>

                                <div className="card-body">
                                    <Form>
                                        <FormGroup>
                                            <Form.Label htmlFor="nombre">Nombre</Form.Label>
                                            <Form.Control type="text" name="nombre" onChange={this.cargardatos}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <DayPickerInput onDayChange={this.obtenerFecha}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Form.Label htmlFor="grupo">Grupo</Form.Label>
                                            <Form.Control as="select" name="grupo"  onChange={this.cargardatos}>
                                                {
                                                    grupos != null ?
                                                        grupos.map((grupo, key) =>
                                                            <option key={key} value={grupo.id}>{grupo.nombreGrupo}</option>
                                                        )
                                                        :
                                                        <option>No hay opciones</option>
                                                }
                                            </Form.Control>
                                        </FormGroup>
                                        <Button className="text-center mb-4 mr-1" color="success" onClick={this.registrar}>
                                            Registrar
                                        </Button>
                                        <Button className="text-center mb-4" variant="secondary" onClick={this.cancelar}>
                                            Cancelar
                                        </Button>
                                    </Form>
                                    {
                                        mensaje.length != 0 ?
                                            <Alert variant="danger" onClose={() => this.setState({mensaje: ""})} dismissible>
                                                {mensaje}
                                            </Alert>
                                            :
                                            null
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
