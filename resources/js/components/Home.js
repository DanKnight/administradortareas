import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import {Api, login, redireccionWeb} from '../utils';
import {Button, Form, FormGroup, Alert} from "react-bootstrap";
import axios from 'axios';

export default class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            mensaje: ""
        };
    }

    // Funcion para agregar los datos de formulario al state
    cargardatos = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        let data = {};
        data[name] = value;
        this.setState(data);
    };

    // Funcion para consumir api de login y obtener un token para le usuario
    login = () => {
        // Generacion de parametros para la peticion
        let parametros = {email: this.state.email, password: this.state.password};

        axios.post(`${Api.url}login`, parametros).then((response) => {
            // Enviar token para almacenarlo en localstorage
            login(response.data.token);
            location.href = redireccionWeb('dashboard');
        }).catch(error => {
            // Mensaje enviado al state para generar alerta
            this.setState({
                mensaje: "Credenciales invalidas"
            });
        });
    };

    render() {
        const mensaje = this.state.mensaje;

        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header text-center">Administrador de Tareas</div>

                            <div className="card-body">
                                <Form>
                                    <FormGroup>
                                        <Form.Label htmlFor="email">Email</Form.Label>
                                        <Form.Control type="email" name="email" onChange={this.cargardatos}/>
                                    </FormGroup>
                                    <FormGroup>
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control type="password" name="password" onChange={this.cargardatos}/>
                                    </FormGroup>
                                    <Button className="text-center mb-4" color="success" onClick={this.login}>
                                        Iniciar Sesión
                                    </Button>
                                </Form>
                                {
                                    mensaje.length != 0 ?
                                        <Alert variant="danger" onClose={() => this.setState({mensaje: ""})} dismissible>
                                            {mensaje}
                                        </Alert>
                                    :
                                    null
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
