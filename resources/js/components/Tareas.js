import React, {Component} from 'react';
import {Api, autenticacionHeader, redireccionWeb} from '../utils';
import {
    Alert,
    Badge,
    Button,
    Card,
    CardColumns,
    Form,
    FormGroup,
    ListGroup,
    ListGroupItem,
    Modal,
    Navbar
} from "react-bootstrap";
import axios from "axios";

export default class Tareas extends Component {
    constructor(props) {
        super(props);

        // Obtencion del array de tareas por medio del props
        const tareasArray = props.tareasArray;

        this.state = {
            tareas: tareasArray,
            mostrar: false,
            mensaje: ""
        };
    }

    // Peticion para completar una tarea
    completarTarea = (id) => {
        // Obtencion de autorizacion para peticiones
        let auth = autenticacionHeader();

        // Generacion de parametros para la peticion
        let parametros = {id: id};

        axios.post(`${Api.url}completa`, parametros, {headers: auth}).then((response) => {
            if (response.data.status == 200){
                // Actualizar el state con las tareas que no han sido completadas
                this.listarTareas();
            }
        }).catch((error) => {
            console.log(error);
        })
    }

    // Peticion para obtener la lista de tareas
    listarTareas = () => {
        // Obtencion de autorizacion para peticiones
        let auth = autenticacionHeader();

        axios.get(`${Api.url}listartareas`, {headers: auth}).then((response) => {
            if (response.data.status == 200){
                // Informacion enviada al state para actualizar la vista
                this.setState({
                    tareas: response.data.data,
                });
            }
        }).catch((error) => {
            console.log(error);
        })
    }

    // Peticion para asignar tarea al usuario
    asignarTarea = () => {
        // Obtencion de autorizacion para peticiones
        let auth = autenticacionHeader();

        // Generacion de parametros para la peticion
        let parametros = {tarea: this.state.tareasid, usuario: this.state.usuario};

        axios.post(`${Api.url}asignar`, parametros ,{headers: auth}).then((response) => {
            if (response.data.status == 200){
                // Redireccionar pagina para actualizar el state del componente dashboard
                document.location.href = redireccionWeb('dashboard');
            } else {
                // Mensaje enviado al state para generar alerta
                this.setState({
                    mensaje: response.data.mensaje
                });
            }
        }).catch((error) => {
            console.log(error);
        })
    }

    // Funcion para agregar los datos de formulario al state
    cargardatos = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        let data = {};
        data[name] = value;
        this.setState(data);
    };

    // Funcion para abrir modal
    abrirModal = (id) => {

        // Mandar mostrar en true para abrir modal y se manda el id de la tarea
        this.setState({
            mostrar: true,
            tareasid: id
        });
    }

    // Funcion para cerra modal
    cerrarModal = () => {
        this.setState({
            mostrar: false
        });
    }

    render() {
        const grupo = this.props.grupos;
        const grupoArray = this.props.grupoArray;
        const tareas = this.state.tareas;
        const mensaje = this.state.mensaje;

        // Generacion de componente de manera dinamica por medio de la funcion map
        return (
            <div className="container">
                {
                    tareas.length != 0 ?
                        <CardColumns>
                            {
                                tareas.map((tarea, key) =>
                                    <Card style={{ width: '14rem' }} key={key}>
                                        <Card.Header className="text-center">Nombre: {tarea.nombre}</Card.Header>
                                        <Card.Body>
                                            <Card.Subtitle className="mb-2 text-muted">Estado: {tarea.status}</Card.Subtitle>
                                            <Card.Text>
                                                <label>Usuarios Asignados</label>
                                                <br></br>
                                                {
                                                    tarea.usuariosAsignados.map((usuario, key) =>
                                                        <Badge variant="light" key={key}>{usuario.nombre}</Badge>
                                                    )
                                                }
                                            </Card.Text>
                                            {
                                                grupo == false ?
                                                    <Button variant="link" onClick={() => this.completarTarea(tarea.id)}>Completar Tarea</Button>
                                                    :
                                                    <Button variant="link" onClick={() => this.abrirModal(tarea.id)}>Asignar Usuario</Button>

                                            }
                                        </Card.Body>
                                        <Card.Footer>
                                            <small className="text-muted">Fecha {tarea.fecha}</small>
                                        </Card.Footer>
                                    </Card>
                                )
                            }
                        </CardColumns>
                        :
                        <h2 className="text-center">No hay tareas</h2>
                }
                <Modal show={this.state.mostrar} onHide={this.cerrarModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Asignar Tarea</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <FormGroup>
                                <Form.Label htmlFor="usuario">Usuario</Form.Label>
                                <Form.Control as="select" name="usuario" onChange={this.cargardatos}>
                                    <option>Seleccione una opcion</option>
                                    {
                                        grupoArray.length != 0 ?
                                            grupoArray.map((usuario, key) =>
                                                <option key={key} value={usuario.id}>{usuario.nombre}</option>
                                            )
                                            :
                                            <option>No hay opciones</option>
                                    }
                                </Form.Control>
                            </FormGroup>
                        </Form>
                        <br></br>
                    </Modal.Body>
                    {
                        mensaje.length != 0 ?
                        <Alert variant="danger" onClose={() => this.setState({mensaje: ""})} dismissible>
                            {mensaje}
                        </Alert>
                        :
                        null
                    }
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.cerrarModal}>
                            Cancelar
                        </Button>
                        <Button variant="primary" onClick={this.asignarTarea}>
                            Guardar
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
