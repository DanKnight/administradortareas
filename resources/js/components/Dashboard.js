import React, {Component} from 'react';
import NavigationBar from "./NavBar";
import axios from "axios";
import {Api, autenticacionHeader} from "../utils";
import Tareas from "./Tareas";
import {Accordion, Button, Card, Form, FormGroup, Modal} from "react-bootstrap";
import Grupo from "./Grupo";

export default class Dashboard extends Component {
    // Declaracion de dato default en el props
    static defaultProps = {
        grupo: null
    }
    constructor(props) {
        super(props);

        this.state = {
            tareas: null,
            grupo: this.props.grupo,
            validar: false,
            mostrar: false
        };

        // Ejecucion de funciones al cargar la pagina
        this.listarTareas();
        this.listargrupos();
    }

    // Peticion para obtener la lista de tareas
    listarTareas = () => {
        // Obtencion de autorizacion para peticiones
        let auth = autenticacionHeader();

        axios.get(`${Api.url}listartareas`, {headers: auth}).then((response) => {
            if (response.data.status == 200){
                // Informacion enviada al state para actualizar la vista
                this.setState({
                    tareas: response.data.data,
                });
            }
        }).catch((error) => {
            console.log(error);
        })
    }

    // Peticion para obtener la lista de grupos
    listargrupos = () => {
        // Obtencion de autorizacion para peticiones
        let auth = autenticacionHeader();

        axios.get(`${Api.url}todosgrupos`, {headers: auth}).then((response) => {
            if (response.data.status == 200){
                // Informacion enviada al state para actualizar la vista
                this.setState({
                    grupo: response.data.data,
                    validar: true,
                });
            }
        }).catch((error) => {
            console.log(error);
        })
    }

    render() {
        let grupos = this.state.grupo;

        // Generacion de componente de manera dinamica por medio de la funcion map
        return (
            <div>
                <NavigationBar grupos={this.state.validar}/>
                {
                    this.state.tareas != null ?
                        <div className="row justify-content-center">
                            <div className="col-md-12">
                                <Card >
                                    <Card.Header className="text-center">Tareas del Grupo</Card.Header>
                                    <br></br>
                                    <Tareas grupos={this.state.validar} tareasArray={this.state.tareas} grupoArray={""}/>
                                </Card>
                            </div>
                        </div>
                        :
                        null
                }
                {
                    grupos != null ?
                        <div className="row justify-content-center">
                            <div className="col-md-12">
                                <Accordion defaultActiveKey="0">
                                    {
                                        grupos.map((grupo) =>
                                            <Card key={grupo.id}>
                                                <Card.Header>
                                                    <Accordion.Toggle as={Card.Header} eventKey={grupo.id} className="text-center">
                                                        {grupo.nombre}
                                                    </Accordion.Toggle>
                                                </Card.Header>
                                                <Accordion.Collapse eventKey={grupo.id}>
                                                    <Card.Body>
                                                        <Card>
                                                            <Card.Header className="text-center">Tareas del Grupo</Card.Header>
                                                            <br></br>
                                                            <Tareas grupos={this.state.validar} tareasArray={grupo.tareasAsignadas} grupoArray={grupo.usuariosGrupo}/>
                                                        </Card>
                                                        <Card>
                                                            <Card.Header className="text-center">Usuarios del Grupo</Card.Header>
                                                            <br></br>
                                                            <Grupo grupoArray={grupo.usuariosGrupo}/>
                                                        </Card>
                                                    </Card.Body>
                                                </Accordion.Collapse>
                                            </Card>
                                            
                                        )
                                    }
                                        
                                </Accordion>
                                
                            </div>
                        </div>
                        :
                        null
                }
            </div>
        );
    };
}
