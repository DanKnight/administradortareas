import React, {Component} from 'react';
import {Api, autenticacionHeader, logout, redireccionWeb} from '../utils';
import {Button, Form, FormGroup, Modal, Navbar} from "react-bootstrap";
import axios from "axios";
import RegistrarTarea from "./RegistrarTarea";

export default class NavigationBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            grupos: null
        }
    }

    // Funcion para cerrar sesion
    logout = () => {
        logout();
    }

    // Funcion para redireccionar a la vista registrar
    registrar = () => {
        location.href = redireccionWeb('registrar');
    }

    render() {
        const gruposExisten = this.props.grupos;
        const grupos = this.state.grupos;

        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Brand>Administrador de Tareas</Navbar.Brand>
                    <Navbar.Toggle/>
                    <Navbar.Collapse className="justify-content-end">
                        {
                            gruposExisten == true ?
                                <Button onClick={this.registrar} variant="secondary" className="mr-2">Agregar Tarea</Button>
                                :
                                null
                        }
                        <Button onClick={this.logout} variant="secondary">Cerrar Sesión</Button>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }
}
